#!/bin/bash
opcode64() { 
  echo $* > tmp.S && gcc -c tmp.S -o tmp.o && objdump -s tmp.o | grep -A9999 '^Contents of' | grep -v Contents | sed 's/  .*//' | tr -d ' ' | cut -c5-
  rm -f tmp.o tmp.S
}
opcode32() {                  
  echo $* > tmp.S && gcc -arch i386 -c tmp.S -o tmp.o && objdump -s tmp.o | grep -A9999 '^Contents of' | grep -v Contents| sed 's/  .*//' | tr -d ' ' | cut -c5-
  rm -f tmp.o tmp.S
}

rm -rf fwind_patched

cp -r fwind fwind_patched

(cat indirect_ref_show_clean.txt | tr '\n' '=' | sed 's/=--=/@/g' | tr '@' '\n' | tr '\t' ' ' | sed 's/=$//' | tr '=' ' ' | sed 's/fwind/@fwind/g' | tr '@' '\n';echo)|grep -v '^$'| while read a b c; do
	echo a $a b $b c $c
	if [ "`echo $c | cut -d ' ' -f 1`"  = calll ];then
		oc=`opcode32 $c`
	else
		oc=`opcode64 $c`
	fi
	echo opc $oc
	oph=`echo $oc | sed 's/\(..\)/\\\x\1/g'`
	nop=`echo $oc | sed 's/../\\\x90/g'`
	echo oph $oph
	echo nop $nop
	a=`echo $a|sed s/fwind/fwind_patched/`
	echo perl -pi -e "s/$oph/$nop/g" $a
	perl -pi -e "s/$oph/$nop/g" $a
	echo unsign/unsign $a
	unsign/unsign $a && mv $a.unsigned $a
done

cd fwind_patched

# this is necessary but the script is not working for some reason, do it manually
#fat_offset64=`otool -f CoreGraphics | grep -m1 offset | awk '{print $2}'`
#for a in initializeBackgroundBlurProgram ; do printf '\x31\xc0\xc3' | dd of=CoreGraphics bs=1 count=3 conv=notrunc seek=$((0x$(otool -tvV CoreGraphics| grep -A1 "_$a:$" | tail -n1 | cut -d "`printf '\t'`" -f1)+$fat_offset64)); done

for i in *;do
me=`otool -L $i  | head -2 | tail -1 | awk '{print $1}'`
mkdir -p `dirname .$me`
mv $i .$me
done
