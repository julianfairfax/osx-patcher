


#include "SIPManager.hpp"

#include <IOKit/IOLib.h>
#include <IOKit/IODeviceTreeSupport.h>
#include <IOKit/system.h>
#include <IOKit/platform/ApplePlatformExpert.h>

#include <kern/debug.h>

#undef super
#define super IOService

OSDefineMetaClassAndStructors(DeviceTreeUpdater, IOService);

extern int installer;

bool
DeviceTreeUpdater::start (IOService *provider)
{
    if (!super::start (provider)) return false;
    
    kern_return_t	result;
    thread_t		thread;
    
    
    this->setSIP(CSR_ALLOW_UNTRUSTED_KEXTS|CSR_ALLOW_UNRESTRICTED_NVRAM|CSR_ALLOW_UNRESTRICTED_DTRACE|CSR_ALLOW_UNRESTRICTED_FS|CSR_ALLOW_TASK_FOR_PID);
    
    result = kernel_thread_start((thread_continue_t)&updateDeviceTree, this, &thread);
    if (result != KERN_SUCCESS)
        return false;
    
    thread_deallocate(thread);
    
    return true;
}

void DeviceTreeUpdater::updateDeviceTree (void *argument)
{
    DeviceTreeUpdater *self = (DeviceTreeUpdater *) argument;
    IOService *provider = self->getProvider ();
    
    // We wait until all the driver matching has finished. The reason is that some of the driver
    // configuration relies on the original "compatible" value.
    provider->waitQuiet ();
    
}
void DeviceTreeUpdater::setSIP(int val)
{
    boot_args *args = (boot_args *)PE_state.bootArgs;
    
    args->flags |= kBootArgsFlagCSRActiveConfig;
    args->csrActiveConfig |= val;
}
